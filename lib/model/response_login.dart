// To parse this JSON DataLogin, do
//
//     final ResponseLogin = ResponseLoginFromJson(jsonString);

import 'dart:convert';

ResponseLogin responseLoginFromJson(String str) => ResponseLogin.fromJson(json.decode(str));

String responseLoginToJson(ResponseLogin DataLogin) => json.encode(DataLogin.toJson());

class ResponseLogin {
    ResponseLogin({
        this.meta,
        this.data,
    });

    Meta? meta;
    DataLogin? data;

    factory ResponseLogin.fromJson(Map<String, dynamic> json) => ResponseLogin(
        meta: Meta.fromJson(json["meta"]),
        data: DataLogin.fromJson(json["data"]),
    );

    Map<String, dynamic> toJson() => {
        "meta": meta?.toJson(),
        "DataLogin": data?.toJson(),
    };
}

class DataLogin {
    DataLogin({
        this.accessToken,
        this.tokenType,
        this.user,
    });

    String? accessToken;
    String? tokenType;
    User? user;

    factory DataLogin.fromJson(Map<String, dynamic> json) => DataLogin(
        accessToken: json["access_token"],
        tokenType: json["token_type"],
        user: User.fromJson(json["user"]),
    );

    Map<String, dynamic> toJson() => {
        "access_token": accessToken,
        "token_type": tokenType,
        "user": user?.toJson(),
    };
}

class User {
    User({
        this.id,
        this.uuid,
        this.name,
        this.email,
        this.gender,
        this.telp,
        this.kotaAsal,
        this.pendidikanTerakhir,
        this.institute,
        this.jurusan,
        this.tanggalLahir,
        this.emailVerifiedAt,
        this.twoFactorSecret,
        this.twoFactorRecoveryCodes,
        this.twoFactorConfirmedAt,
        this.createdAt,
        this.updatedAt,
    });

    int? id;
    String? uuid;
    String? name;
    String? email;
    String? gender;
    String? telp;
    String? kotaAsal;
    String? pendidikanTerakhir;
    String? institute;
    String? jurusan;
    DateTime? tanggalLahir;
    dynamic emailVerifiedAt;
    dynamic twoFactorSecret;
    dynamic twoFactorRecoveryCodes;
    dynamic twoFactorConfirmedAt;
    DateTime? createdAt;
    DateTime? updatedAt;

    factory User.fromJson(Map<String, dynamic> json) => User(
        id: json["id"],
        uuid: json["uuid"],
        name: json["name"],
        email: json["email"],
        gender: json["gender"],
        telp: json["telp"],
        kotaAsal: json["kota_asal"],
        pendidikanTerakhir: json["pendidikan_terakhir"],
        institute: json["institute"],
        jurusan: json["jurusan"],
        tanggalLahir: DateTime.parse(json["tanggal_lahir"]),
        emailVerifiedAt: json["email_verified_at"],
        twoFactorSecret: json["two_factor_secret"],
        twoFactorRecoveryCodes: json["two_factor_recovery_codes"],
        twoFactorConfirmedAt: json["two_factor_confirmed_at"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "uuid": uuid,
        "name": name,
        "email": email,
        "gender": gender,
        "telp": telp,
        "kota_asal": kotaAsal,
        "pendidikan_terakhir": pendidikanTerakhir,
        "institute": institute,
        "jurusan": jurusan,
        "tanggal_lahir": "${tanggalLahir?.year.toString().padLeft(4, '0')}-${tanggalLahir?.month.toString().padLeft(2, '0')}-${tanggalLahir?.day.toString().padLeft(2, '0')}",
        "email_verified_at": emailVerifiedAt,
        "two_factor_secret": twoFactorSecret,
        "two_factor_recovery_codes": twoFactorRecoveryCodes,
        "two_factor_confirmed_at": twoFactorConfirmedAt,
        "created_at": createdAt?.toIso8601String(),
        "updated_at": updatedAt?.toIso8601String(),
    };
}

class Meta {
    Meta({
        this.code,
        this.status,
        this.message,
    });

    int? code;
    String? status;
    String? message;

    factory Meta.fromJson(Map<String, dynamic> json) => Meta(
        code: json["code"],
        status: json["status"],
        message: json["message"],
    );

    Map<String, dynamic> toJson() => {
        "code": code,
        "status": status,
        "message": message,
    };
}
