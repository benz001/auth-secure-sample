import 'package:auth_secure_sample/helper/encrypt_helper.dart';
import 'package:auth_secure_sample/helper/token_helper.dart';
import 'package:flutter/material.dart';

class EncryptSamplePage extends StatefulWidget {
  const EncryptSamplePage({Key? key}) : super(key: key);

  @override
  State<EncryptSamplePage> createState() => _EncryptSamplePageState();
}

class _EncryptSamplePageState extends State<EncryptSamplePage> {
  TokenHelper _tokenHelper = TokenHelper();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Container(
        color: Colors.grey,
        width: double.infinity,
        height: double.infinity,
        padding: const EdgeInsets.symmetric(horizontal: 20),
        child: Column(
          children: [
            Container(
                width: double.infinity,
                height: 45,
                margin: const EdgeInsets.only(top: 50),
                color: Colors.transparent,
                child: ElevatedButton(
                    onPressed: () {
                      final ecryptResult = EncryptHelper.encryptWithAES(
                          'my_key_token_my_key_token_my_key',
                          '94|g3a7lAg0SwSeP9NoSMIodIy5ztoe3xufjlhrEu5N');
                      print(ecryptResult.base64);
                    },
                    child: const Text('Encrypt'))),
            const SizedBox(
              height: 10,
            ),
            Container(
                width: double.infinity,
                height: 45,
                color: Colors.transparent,
                child: ElevatedButton(
                    onPressed: () {
                      final ecryptResult = EncryptHelper.encryptWithAES(
                          'my_key_token_my_key_token_my_key',
                          '"Fallback! I repeat fallback to sector 12!!"');
                      final decryptResult = EncryptHelper.decryptWithAES(
                          'my_key_token_my_key_token_my_key', ecryptResult);
                      print(decryptResult);
                    },
                    child: const Text('Dencrypt'))),
            const SizedBox(
              height: 10,
            ),
            Container(
                width: double.infinity,
                height: 45,
                margin: const EdgeInsets.only(top: 50),
                color: Colors.transparent,
                child: ElevatedButton(
                    onPressed: () {
                      _tokenHelper.saveToken('gfhhhgfhgdhdhgdhgd');
                    },
                    child: const Text('Save Token'))),
            const SizedBox(
              height: 10,
            ),
            Container(
                width: double.infinity,
                height: 45,
                color: Colors.transparent,
                child: ElevatedButton(
                    onPressed: () {
                      print(_tokenHelper.getToken());
                    },
                    child: const Text('Get Token')))
          ],
        ),
      ),
    );
  }
}
