import 'dart:convert';
import 'dart:math';

import 'package:http/http.dart' as http;

class APIService {
  Future<http.Response> loginProcess(String email, String password) async {
    final response = await http.post(
        Uri.parse('https://api-lpdp.bas-it.id/lpdp-app/public/api/login'),
        headers: {"Content-Type": "application/json", "Accept": "*/*"},
        body: jsonEncode({"email": email, "password": password}));
    try {
      return response;
    } catch (e) {
      return response;
    }
  }
}
