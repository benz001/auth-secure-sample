import 'package:flutter/material.dart';

class CostumizeTextfield extends StatelessWidget {
  final TextEditingController controller;
  final String? hintText;
  final bool? obsecureText;
  const CostumizeTextfield(
      {Key? key, required this.controller, this.hintText, this.obsecureText})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextField(
      controller: controller,
      obscureText: obsecureText ?? false,
      decoration: InputDecoration(
        filled: true,
        hintText: hintText ?? '',
        fillColor: Colors.white,
        enabledBorder: OutlineInputBorder(
          borderSide:
              const BorderSide(width: 1, color: Colors.black), //<-- SEE HERE
          borderRadius: BorderRadius.circular(10),
        ),
      ),
    );
  }
}
