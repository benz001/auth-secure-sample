import 'package:auth_secure_sample/bloc/event/login_event.dart';
import 'package:auth_secure_sample/bloc/handle_api/login_bloc.dart';
import 'package:auth_secure_sample/bloc/state/login_state.dart';
import 'package:auth_secure_sample/helper/token_helper.dart';
import 'package:auth_secure_sample/model/response_login.dart';
import 'package:auth_secure_sample/ui/home_page.dart';
import 'package:auth_secure_sample/ui/widget/costumize_textfield.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final emailController = TextEditingController(text: '');
  final passwordController = TextEditingController(text: '');
  final LoginBloc _loginBloc = LoginBloc();
  final TokenHelper _tokenHelper = TokenHelper();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Login Page'),
      ),
      body: Container(
        width: double.infinity,
        height: double.infinity,
        padding: const EdgeInsets.symmetric(horizontal: 20),
        color: Colors.grey[300],
        child: BlocConsumer<LoginBloc, LoginState>(
            bloc: _loginBloc,
            listener: (context, state) {
              print('state is: $state');
              if (state is LoginSuccess) {
                print(responseLoginToJson(state.responseLogin));
                _tokenHelper.saveToken(state.responseLogin.data!.accessToken!);
                Future.delayed(Duration(seconds: 1), () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) {
                    return const HomePage();
                  }));
                });
              }

              if (state is LoginError) {
                Fluttertoast.showToast(
                    msg: responseLoginToJson(state.responseLogin));
              }
            },
            builder: (context, state) {
              return Column(
                children: [
                  const SizedBox(
                    height: 20,
                  ),
                  Container(
                    width: double.infinity,
                    height: 50,
                    color: Colors.transparent,
                    child: CostumizeTextfield(
                      controller: emailController,
                      hintText: 'Enter Email',
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Container(
                    width: double.infinity,
                    height: 50,
                    color: Colors.transparent,
                    child: CostumizeTextfield(
                      controller: passwordController,
                      hintText: 'Enter Password',
                      obsecureText: true,
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Builder(builder: (context) {
                    if (state is LoginLoading) {
                      return Container(
                          width: double.infinity,
                          height: 45,
                          color: Colors.transparent,
                          child: ElevatedButton(
                              onPressed: () {},
                              child: const Center(
                                child: CircularProgressIndicator(
                                    color: Colors.white),
                              )));
                    } else {
                      return Container(
                          width: double.infinity,
                          height: 45,
                          color: Colors.transparent,
                          child: ElevatedButton(
                              onPressed: () {
                                if (emailController.text.isEmpty ||
                                    passwordController.text.isEmpty) {
                                  Fluttertoast.showToast(
                                      msg: 'Texfield is required');
                                } else {
                                  _loginBloc.add(LoginPost(
                                      email: emailController.text,
                                      password: passwordController.text));
                                }
                              },
                              child: const Text('LOGIN')));
                    }
                  })
                ],
              );
            }),
      ),
    );
  }
}
