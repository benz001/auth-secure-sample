import 'package:auth_secure_sample/helper/token_helper.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final TokenHelper _tokenHelper = TokenHelper();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Home Page'),
      ),
      body: Container(
        width: double.infinity,
        height: double.infinity,
        color: Colors.grey[300],
        alignment: Alignment.center,
        child: Container(
            width: double.infinity,
            height: 45,
            color: Colors.transparent,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: ElevatedButton(
                  onPressed: () async{
                    String token = await _tokenHelper.getToken();
                    print(token);
                    Fluttertoast.showToast(msg: token);
                  },
                  child: const Text('GET TOKEN')),
            )),
      ),
    );
  }
}
